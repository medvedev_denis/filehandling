import java.io.*;


public class FileHandling {


    private static int num1;
    private static int num2;
    private static int result;
    private static char lineChar;


    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader("Input.txt"));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("Output.txt"));
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            processingAndWriting(line, bufferedWriter);
        }
        bufferedReader.close();
        bufferedWriter.close();
        System.out.println("Процесс выполнен успешно!");
    }


    public  static void processingAndWriting(String line, BufferedWriter bW) throws IOException {
        String finString;
        if (line.equals("...")) {
            bW.write(line);
            return;
        }
        else
            strReader(line);
        char[] possibleSigns = {'+', '-', '*', '/'};
        for (int i = 0;i < 4 ; i++) {
            int flag = 1;
            if (lineChar == possibleSigns[i]) {
                flag = 1;
                i = 4;
            } else {
                flag = 0;
            }
            switch (flag) {
                case (1):
                    switch (lineChar) {
                        case ('+'):
                            result = add(num1, num2);
                            break;
                        case ('-'):
                            result = subs(num1, num2);
                            break;
                        case ('*'):
                            result = mult(num1, num2);
                            break;
                        case ('/'):
                            result = div(num1, num2);
                            break;
                    }
                default:
            }
        }
        finString = line + " = " + result;
        bW.write(finString + "\n");
    }

    public static void strReader(String line) {
        String[] lineComponents = line.split(" ");
        String[] lineComponentsNums = {lineComponents[0], lineComponents[2]};
        int[] intNums = new int[2];
        for (int i = 0; i < lineComponentsNums.length; i++) {
            intNums[i] = Integer.valueOf(lineComponentsNums[i]);
        }
        lineChar = lineComponents[1].charAt(0);
        num1 = intNums[0];
        num2 = intNums[1];
    }


    public static int add(int num1,int num2){
        result=num1+num2;
        return result;
    }

    public static int subs(int num1,int num2){
        result=num1-num2;
        return result;
    }

    public static int mult(int num1,int num2){
        result=num1*num2;
        return result;
    }

    public static int div(int num1,int num2){
        result=num1/num2;
        return result;
    }

}
